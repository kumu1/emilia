import Crypto from 'crypto';
export const HashPassword = (password, key, salt) => {
  const hashed = Crypto.createHmac("sha256", key + salt)
    .update(password)
    .digest("hex");
  return hashed;
}
