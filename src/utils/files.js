import {arrayIsEmpty, isUndefined} from "./functions";

const getOnlyFilesData = (data) => {
	// Helper function to get just the files in query object
	const entries = Object.entries(data);
	return entries.filter( entry => !isUndefined(entry[1]['isFile']))
}

const getFileType = (filename) => {
	// Helper function to get the filetype of a filename
	return filename.split('.').pop();
}

const checkIfFileTypeIsIncluded = (filetype, types) => {
	// Helper function to check if filetype in included in an array of types
	return types.includes(filetype)
}

const createFileReferenceInDB = async (filetype, collection, emilia) => {
	// Helper function to create file reference in DB
	return emilia.createQuery({collection, data: { filetype }})
}

export const handleFiles = async (data, emilia) => {
	return new Promise(async (resolve, reject) => {
		// DB === database

		// A1- Get the files in the query object
		// A2- If There is no files, return the same query object (data)
		// A3- If a file exist - handleFiles create an image or file object in the database
		// A3.1- Replace the file entry in the query object (data) for the id of the object created ->
		//			-> in the database

		// A1 ->
		const files = getOnlyFilesData(data)
		// A2 ->
		if(arrayIsEmpty(files)) return data

		// A3 ->
		////////
		// B1- IF file is an image with the filetype included in <imageTypesAccepted> ->
		// 		-> we create an image reference in the DB
		// B2- ELSE IF filetype is included in <fileTypesAccepted> we create a file reference in the DB
		// B3- ELSE when the filetype is not included in images or files throw an error

		const imageTypesAccepted = ['png', 'jpg', 'svg'] // <imageTypesAccepted>
		const fileTypesAccepted = [] // <fileTypesAccepted>
		const fileEntriesWithReference = {}

		for(const fileIndex in files) {
			const fileKey = files[fileIndex][0]
			const fileValue = files[fileIndex][1]
			const filetype = getFileType(fileValue.filename)
			// B1 ->
			if(checkIfFileTypeIsIncluded(filetype, imageTypesAccepted)){
				try {
					const fileReference = await createFileReferenceInDB(filetype, 'images', emilia)
					fileEntriesWithReference[fileKey] = fileReference.id
				} catch (e) {
					reject(e)
				}
			}
			// B2 ->
			else if(checkIfFileTypeIsIncluded(filetype, fileTypesAccepted)){
				try {
					const fileReference = await createFileReferenceInDB(filetype, 'files', emilia)
					fileEntriesWithReference[fileKey] = fileReference.id
				} catch (e) {
					reject(e)
				}
			}
			// B3 ->
			else {
				reject('FileType is not included')
			}
		}
		// A3.1 ->
		resolve({...data, ...fileEntriesWithReference})
	})
}
