import {errors_message} from "./utils/errors";
import {GenerateString} from "./utils/functions";
import {HashPassword} from "./utils/encryptation";
import {ObjectID} from "mongodb";
import {getEmiliaConfig} from "./index";

export function createRecoverPassword ({ email, password, key, userId }) {
  const check = [email, password];
  let isValid = true;
  check.forEach(dta => {
    if(typeof dta !== "string"){
      isValid = false;
      return
    }
    if(dta.length < 1){
      isValid = false;
    }
  })
  const salt = GenerateString(32);
  const hashPassword = HashPassword(password, key, salt)
  let dateToSave = new Date(Date.now());
  dateToSave.setDate(dateToSave.getDate() + 2);

  if(isValid){
    return {
      email,
      password: hashPassword,
      salt,
      user_id: userId,
      date: dateToSave.toISOString()
    };
  } else {
    return false;
  }
}

export async function sendResetPassword (query) {
  return new Promise(async (resolve, reject) => {
    const {db, secretKeyHash} = getEmiliaConfig();
    try {
      const checkUserExistQuery = {
        "$or": [{email: query.data.email}, {user: query.data.user}]
      }
      const checkUserExist = await db.collection("users").find(checkUserExistQuery);
      if(checkUserExist.length > 0){
        const recoverPassQuery = {
          email: checkUserExist[0].email,
          password: query.data.password,
          userId: checkUserExist[0]._id
        }
        const recoverPassword = createRecoverPassword({
          ...recoverPassQuery, key: secretKeyHash
        })
        if(!recoverPassword){
          reject({
            errors:true,
            err_message: errors_message.error_recover_password_parameters_not_valid
          })
          return
        }
        try {
          const result = await db.collection("recover_password").insertOne(recoverPassword);
          if (result.insertedCount >= 1){
            try {
              await this.sendAuthenticationWithId(result.ops[0]._id.toString())
              console.dir(`Para cambiar la contraseña de este usuario debe ingresar a http://localhost:3000/auth-reset-password?q=${result.ops[0]._id}&e=${recoverPassword.email}  `)
              resolve()
            } catch (e) {
              reject({
                errors: true,
                err_message: errors_message.error_when_creating_object
              })
            }
          } else {
            reject({
              errors:true,
              err_message: errors_message.error_when_creating_object
            })
          }
        } catch (e) {
          reject({
            errors:true,
            err_message: errors_message.error_when_creating_object
          })
        }
      }

    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.error_when_findByValues
      })
    }
  })
}
export async function checkRecoverPassword (query) {
  return new Promise(async (resolve, reject) => {
    const { db } = getEmiliaConfig();
    try {
      const recoverPasswordQuery = {
        _id: new ObjectID(query.query),
        email: query.email
      }
      const recoverPassword = await db.collection("recover_password").findOne(recoverPasswordQuery);
      if(recoverPassword === null) {
        reject({
          errors:true,
          err_message: errors_message.wrong_recover_password_by_email
        })
        return
      }
      const checkDate = new Date(recoverPassword.date);
      const now = new Date(Date.now())
      if(now.getTime() > checkDate.getTime()) {
        try {
          const deleteSesionQuery = await db.collection("recover_password").deleteOne(recoverPasswordQuery);
          if(deleteSesionQuery.result.ok < 1){
            reject({
              errors: true,
              err_message: errors_message.error_when_querying_delete
            })
          }
        } catch (e) {
          reject({
            errors: true,
            err_message: errors_message.error_when_querying_delete
          })
        }
        reject({
          errors: true,
          err_message: errors_message.expired_recover_password_code
        })
        return
      }
      const filter = { _id: new ObjectID(recoverPassword.user_id) };
      const updateDocument = {
        $set: {
          password: recoverPassword.password,
          salt: recoverPassword.salt,
          attacked: false,
          locked_account_end_date: '',
          wrongIntentsSingIn: 0
        },
      };
      try {
        const changeUserPassword = await db.collection("users").updateOne(filter, updateDocument);
        if(changeUserPassword.result.ok >= 1){
          try {
            const deleteSesionQuery = await db.collection("recover_password").deleteOne(recoverPasswordQuery);
            if(deleteSesionQuery.result.ok < 1){
              resolve()
            }
          } catch (e) {
            reject({
              errors: true,
              err_message: errors_message.error_when_querying_delete
            })
          }
        } else {
          reject({
            errors:true,
            err_message: errors_message.error_when_querying_update
          })
        }
      } catch (e) {
        reject({
          errors:true,
          err_message: errors_message.error_when_querying_update
        })
      }
    } catch (e) {
      reject({
        errors:true,
        err_message: errors_message.error_when_findByValues
      })
    }
  })
}
