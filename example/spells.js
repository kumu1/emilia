import {Waifu} from "./models/rem";

const exampleSpell = async ({ data, req, res }) => {
	const waifu = new Waifu({
		seiyuu: 'withImage',
		kawaii: false,
		image: {
			isFile: true,
			filename: 'testFile.png'
		}
	})
	const waifuImageExample = await waifu.create()
	res.json(waifuImageExample)
}

export const spellsManifest = {
	example: exampleSpell
}
